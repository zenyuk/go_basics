package main

/*

What is wrong with the code:

* No comment for the package, need to read and understand the whole file
* Name of the function "worker" could be more descriptive
* Function "main" better be the first one from the top
* No tests or example-tests
* Giving the variable "jobs2" name in the same pattern as channel ("jobs") confuses
* Would be better to extract anonymous function - line 3 in the "main" function
* Could use defer for "close" functions
* Use sync.WaitGroup to sync goroutines
* In current implementation the program "panics" due to sending to a closed channel
* Assigning to the index variable inside "range": i = i + 1
* Variable "jobs2" is not required, same as redundant "for" loop
* Changing index "i" in a "for" body, loop creates complexity
* Changing index "i" inside a goroutine making it concurrent memory, in this program not protected by a mutex; creates race conditions
* Hard to guess the original intention for this line: "i += 99". Increase parent scope "i" and skip 99 for all goroutines?
* For the "worker" function, "jobs" channel is used for reading, then why all those nonsense with assigning to "j" in all "cases of switch"


Response from Kablamo:
- "close could fail and overflow error such as sum"

*/

import "fmt"

func worker(id int, jobs <-chan int, results chan<- int) {
	for j := range jobs {
		go func() {
			switch j % 3 {
			case 0:
				j = j * 1
			case 1:
				j = j * 2
				results <- j * 2
			case 2:
				results <- j * 3
				j = j * 3
			}
		}()
	}
}
func main() {
	jobs := make(chan int)
	results := make(chan int)
	for i := 1; i <= 1000000000; i++ {
		go func() {
			if i%2 == 0 {
				i += 99
			}
			jobs <- i
		}()
	}
	close(jobs)
	jobs2 := []int{}
	for w := 1; w < 1000; w++ {
		jobs2 = append(jobs2, w)
	}
	for i, w := range jobs2 {
		go worker(w, jobs, results)
		i = i + 1
	}
	close(results)
	var sum int32 = 0

	for r := range results {
		sum += int32(r)
	}
	fmt.Println(sum)
}
