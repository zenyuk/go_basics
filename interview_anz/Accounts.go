package main

import (
	"strconv"
	"strings"
)

type Accounts []int

func (a Accounts) sameAs(other Accounts) bool {
	b := strings.Builder{}
	for _, id := range a {
		b.WriteString(strconv.Itoa(id))
	}
	s1 := b.String()
	b.Reset()
	for _, id := range other {
		b.WriteString(strconv.Itoa(id))
	}
	s2 := b.String()
	return s1 == s2
}
