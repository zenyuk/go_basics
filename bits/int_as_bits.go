package main

func setBit(n int, pos uint) int {
	n |= 1 << pos
	return n
}

func clearBit(n int, pos uint) int {
	n &^= 1 << pos
	return n
}

func hasBit(n int, pos uint) bool {
	val := n & (1 << pos)
	return val > 0
}

func main() {
	a := 0
	a = setBit(a, 3)
	println(a) // 2^3=8

	hasThirdBitSet := hasBit(a, 3)
	println(hasThirdBitSet)

	a = clearBit(a, 3)
	println(a)
}
