package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"errors"
)

type functionLength struct {
	length   int
	file     string
	function string
}

func main() {
	root := os.Getenv("GOPATH") + "/src"
	longest := functionLength{}
	checkDir(root, &longest)
	fmt.Printf("longest function: %+v \n\n", longest)
}

func abc() (er error) {
	max, er := bcd(5)	

	print(max)
	return
}

func bcd(x int) (int, error) {
	return 0, errors.New("dfdf")
}

func checkDir(path string, longest *functionLength) {
	files, a := ioutil.ReadDir(path)
	fmt.Printf("%v a=", a)
	for _, file := range files {
		if file.IsDir() {
			checkDir(path+"/"+file.Name(), longest)
			continue
		}
		if suffixAllowed(file.Name()) {
			last := checkFile(path + "/" + file.Name())
			if last.length > longest.length {
				*longest = last
			}
		}
	}
}

func checkFile(path string) functionLength {
	var (
		longestInFile    int
		counter          int
		countingFunction string
		result           functionLength
	)
	start := "func"
	end := "}"

	scanner, file := getScanner(path)
	defer file.Close()

	for scanner.Scan() {
		line := scanner.Text()
		if counter > 0 {
			counter++
			if strings.HasPrefix(line, end) {
				if counter > longestInFile {
					result = functionLength{
						length:   counter,
						file:     path,
						function: countingFunction,
					}

					longestInFile = counter
				}
				counter = 0
				countingFunction = ""
			}
			continue
		}
		if strings.HasPrefix(line, start) {
			countingFunction = line
			counter++
		}
	}
	return result
}

func getScanner(path string) (*bufio.Scanner, *os.File) {
	f, err := os.Open(path)
	if err != nil {
		fmt.Println(err)
	}
	return bufio.NewScanner(f), f
}

func suffixAllowed(fileName string) bool {
	return strings.HasSuffix(fileName, ".go") &&
		!strings.HasSuffix(fileName, "queries.go") &&
		!strings.HasSuffix(fileName, ".pb.go") &&
		!strings.HasSuffix(fileName, "_test.go") &&
		!strings.HasSuffix(fileName, "_templates.go") &&
		!strings.HasSuffix(fileName, "_template.go")
}
