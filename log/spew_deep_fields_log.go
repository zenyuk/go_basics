package main

import (
	"fmt"
	"github.com/davecgh/go-spew/spew"
)

type abc struct {
	a int
	b *abc
	c string
}

func main() {
	b := abc{a: 0}
	s := abc{a: 1, b: &b, c: ""}
	d := fmt.Sprintf("struct: %+v", s)
	println(d)
	d2 := spew.Sdump(s)
	println(d2)
}
