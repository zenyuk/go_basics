package main

import (
	"errors"
	"github.com/rs/zerolog/log"
)

func main() {
	log.Error().Msg("log.Error()")
	log.Err(errors.New("log.err")).Msg("log.Err")
	log.Warn().Err(errors.New("log.err")).Msg("log.Warn().Msg")
}
