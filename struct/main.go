package main

import "fmt"

type abc struct {
	a int
	// case sensitive fields
	A int
}

func main() {
	my := abc{1, 3}
	fmt.Printf("%+v\n", my)
}

