package main

/* Sequence
variable initialization
init
program
*/

var packageVar = f()

func init() {
	println("init")
}

func main() {
	println("program")
}

func f() bool {
	println("variable initialization")
	return true
}

