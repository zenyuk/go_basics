package main

import "fmt"

type Teapot struct {
	SomeData string
}

func (t *Teapot) Pour(s string) {
	fmt.Println("Im a little teapot,", s)
}

func (t *Teapot) DieIfNil(s string) {
	fmt.Println("Im about to deliberately die if nil, by dereferencing myself")
	fmt.Println(t.SomeData)
	fmt.Println(s)
}

func main() {
	var nilT *Teapot //  nil pointer to a teapot struct

	nilT.Pour("with a nil instance address, but my methods still work, believe it or not :)")
	
	nilT.DieIfNil("Kamakaze attack")
}
