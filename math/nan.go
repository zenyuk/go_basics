package main

import (
	"fmt"
	"math"
)

func main() {
	var nvalue float64
	nvalue = math.Sqrt(-1)
	fmt.Printf("value: %v\n", nvalue)

	if nvalue == math.NaN() {
		print("will not get here")
	} else {
		print("NaN is NOT equal to NaN")
	}
}

