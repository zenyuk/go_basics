package main

import (
	"fmt"
	"math"
)

func main() {
	div := 29 / 5

	var x float64 = 5
	mod := math.Mod(29, x)

	fmt.Printf("div: %v; mod: %v\n", div, mod)
}
