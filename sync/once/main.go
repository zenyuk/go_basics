package main

import "sync"

func main() {
	var once sync.Once
	var ch = make(chan (bool))
	onceBody := func() {
		println("bla")
	}
	for i := 0; i < 10; i++ {
		go func() {
			once.Do(onceBody)
			ch <- true
			println("sent")
		}()
	}
	for j := 0; j < 10; j++ {
		<-ch
		println("received")
	}
}
