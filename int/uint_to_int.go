package main

import (
	"fmt"
	"math"
)

func main() {
	var u uint = math.MaxInt64 + 1
	// int overflow
	var i int = int(u)

	// output: uint: 9223372036854775808; int: -9223372036854775808
	fmt.Printf("uint: %d; int: %d\n", u, i)
}
