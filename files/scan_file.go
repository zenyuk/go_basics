package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

// CountInAllFiles stores how many times and in which files something (e.g. a string) was found
type CountInAllFiles struct {
	Count uint
	Files []string
}

func (c *CountInAllFiles) AddFileCount(file string) {
	c.Count++
	for _, f := range c.Files {
		if f == file {
			return
		}
	}
	c.Files = append(c.Files, file)
}

func (c *CountInAllFiles) PrintCountInFiles() string {
	result := strings.Builder{}
	result.WriteString("count: ")
	result.WriteString(strconv.Itoa(int(c.Count)))
	result.WriteString(" , files:")
	for _, f := range c.Files {
		result.WriteString(f)
		result.WriteString("\n")
	}
	return result.String()
}

func main() {
	fmt.Printf("start \n")
	allStrings := map[string]*CountInAllFiles{}
	files := getFiles()
	for _, filePath := range files {
		f, err := os.Open(filePath)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}
		scanner := bufio.NewScanner(f)
		for scanner.Scan() {
			l := scanner.Text()
			s, exist := allStrings[l]
			if exist {
				s.AddFileCount(filePath)
			} else {
				allStrings[l] = &CountInAllFiles{1, []string{filePath}}
			}
		}
	}
	for s, c := range allStrings {
		if c.Count > 1 {
			fmt.Printf("string: %s, details: %s", s, c.PrintCountInFiles())
		}
	}
}

func getFiles() []string {
	return []string{"/home/u/temp/text.txt", "/home/u/temp/dupl.txt", "/home/u/temp/nodup.txt"}
}
