package main

import (
	"fmt"
	"io/ioutil"
	"strconv"
)

/*
	Reading file like:
	12345678
	55446688
*/
func main() {
	bytes, err := ioutil.ReadFile("/tmp/temp.txt")
	onErrorPanic(err)
	i1, _ := strconv.Atoi(string(bytes[0]))
	i2 := string(bytes[0])

	// straight conversion gives incorrect value 49 instead of 1
	i3 := int(bytes[0])

	fmt.Printf("first: %v\nsecond: %v\nthird: %v\n", i1, i2, i3)

	/*
	~ xxd -b /tmp/temp.txt
	00000000: 00110001 00110010 00110011 00110100 00110101 00110110  123456
	00000006: 00110111 00111000 00001010 00110101 00110101 00110100  78.554
	0000000c: 00110100 00110110 00110110 00111000 00111000 00001010  46688.

	why file encodes 1 as 00110001 ?
	00110001 => 1

	because in ascii encoding "digit 1" has code "49"

	~ file -i /tmp/temp.txt
	/tmp/temp.txt: text/plain; charset=us-ascii

	ascii codes for digits:
	0 - 48 - DIGIT ZERO
	1 - 49 - DIGIT ONE
	2 - 50 - DIGIT TWO
	3 - 51 - DIGIT THREE
	4 - 52 - DIGIT FOUR
	5 - 53 - DIGIT FIVE
	6 - 54 - DIGIT SIX
	7 - 55 - DIGIT SEVEN
	8 - 56 - DIGIT EIGHT
	9 - 57 - DIGIT NINE
	*/

	b := byte(0b00110001)
	i4 := int(b)
	println(i4)
}

func onErrorPanic(err error) {
	if err != nil {
		panic(err)
	}
}