package main

import (
	"fmt"
	"strconv"
	"time"
)

func main() {
	withBuffer()
    withoutBuffer()
}

func withBuffer() {
	c := make(chan int, 5)
	c <- 1
	c <- 2
	go spin(c)
	c <- 3
	time.Sleep(5 * time.Second)
	c <- 4
	c <- 5
	c <- 6
	c <- 7
	time.Sleep(15 * time.Second)
}

func withoutBuffer() {
	c := make(chan int)
	go spin(c)
	c <- 3
	time.Sleep(5 * time.Second)
	c <- 4
	c <- 5
	c <- 6
	c <- 7
	time.Sleep(15 * time.Second)
}

func spin(c <-chan int) {
    // range will block routine, in which it's running. The blocked routine will be put away from task scheduler.
	for i := range c {
		println("sleep start")
		time.Sleep(1 * time.Second)
		s := strconv.Itoa(i)
		println("got: " + s)
	}
}
