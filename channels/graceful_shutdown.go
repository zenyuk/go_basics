package main

import (
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	println("started")
	kafkaError := make(chan struct{})

	osExit := prepareGracefulShutdown()
	//go activity()

	select {
	case <-kafkaError:
	case <-osExit:
	}

	time.Sleep(time.Second)
	println("exiting")
}

func activity() {
	for {
		println("doing something")
		time.Sleep(time.Second)
	}
}

func prepareGracefulShutdown() chan os.Signal {
	osSignals := make(chan os.Signal)
	signal.Notify(osSignals, syscall.SIGINT, syscall.SIGTERM)
	return osSignals
}
