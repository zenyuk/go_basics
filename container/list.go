package main

import (
	"container/list"
	"fmt"
)

/*
type Element
    func (e *Element) Next() *Element
    func (e *Element) Prev() *Element
type List
    func New() *List
    func (l *List) Back() *Element
    func (l *List) Front() *Element
    func (l *List) Init() *List
    func (l *List) InsertAfter(v interface{}, mark *Element) *Element
    func (l *List) InsertBefore(v interface{}, mark *Element) *Element
    func (l *List) Len() int
    func (l *List) MoveAfter(e, mark *Element)
    func (l *List) MoveBefore(e, mark *Element)
    func (l *List) MoveToBack(e *Element)
    func (l *List) MoveToFront(e *Element)
    func (l *List) PushBack(v interface{}) *Element
    func (l *List) PushBackList(other *List)
    func (l *List) PushFront(v interface{}) *Element
    func (l *List) PushFrontList(other *List)
    func (l *List) Remove(e *Element) interface{}
*/

func main() {
	l := list.New()
	l.Init()

	// insert
	start := l.PushFront("a")
	l.PushBack("c")
	l.InsertAfter("b", start)

	// read and convert
	str := start.Value.(string)
	fmt.Printf("got back: %v\n", str)

	// delete
	l.Remove(start)
	for el := l.Front(); el != nil; el = el.Next() {
		fmt.Printf("el: %v\n", el.Value)
	}
}