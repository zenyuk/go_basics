package performance

func fibonacciChannels(n int) int {
	el := make(chan int)
	quit := make(chan bool)
	go fib(el, quit)
	var result int
	for i := 0; i <= n; i++ {
		result = <-el
	}
	quit <- true
	return result
}

func fib(i chan<- int, quit <-chan bool) {
	a, b := 0, 1
	for {
		select {
		case i <- a:
			a, b = b, a+b
		case <- quit:
			return
		}
	}
}
