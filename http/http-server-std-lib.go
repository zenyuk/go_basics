package main

import (
	"fmt"
	"net/http"
	"time"
)

func defaultHandler(w http.ResponseWriter, r *http.Request) {
	msg := " Request is being processed"
	println(time.Now().String() + msg)
	fmt.Fprintf(w, msg)
}

func main() {
	http.HandleFunc("/", defaultHandler)
	http.ListenAndServe(":8080", nil)
}